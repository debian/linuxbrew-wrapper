#!/bin/sh
# Linuxbrew wrapper for Debian Package
#
# * This script should be able to work outside of the Debian package.
#
# * If detected linuxbrew instance
#      execute the existing brew with optional arguments
#   else 
#      if found linuxbrew install script
#         install linuxbrew, then execute brew with optional arguments
#      else
#         download linuxbrew and install it from scratch, then execute
#      end
#   end
#
# Copyright: 2015-2016 Lumin <cdluminate@gmail.com>
# License:   BSD-2-Clause
set -e

# --- [ Check/Sync Environment ] ---
# here I use LINUXBREW_PREFIX as the primary environment variable,
# HOMEBREW_PREFIX here is for compatibility.
if [ -z ${LINUXBREW_PREFIX} ] && [ -z ${HOMEBREW_PREFIX} ]; then
  # when both ENV are not set, use default.
  if [ -d /home/linuxbrew/.linuxbrew ] && [ -w /home/linuxbrew/.linuxbrew ]; then
    export LINUXBREW_PREFIX="/home/linuxbrew/.linuxbrew"
  else
    export LINUXBREW_PREFIX="${HOME}/.linuxbrew"
  fi
  export HOMEBREW_PREFIX=${LINUXBREW_PREFIX}
elif [ -z ${HOMEBREW_PREFIX} ];then
  # LINUXBREW_PREFIX is set. sync.
  export HOMEBREW_PREFIX=${LINUXBREW_PREFIX}
elif [ -z ${LINUXBREW_PREFIX} ];then
  # HOMEBREW_PREFIX is set. sync.
  export LINUXBREW_PREFIX=${HOMEBREW_PREFIX}
else #[ ! -z ${LINUXBREW_PREFIX} ] && [ ! -z ${HOMEBREW_PREFIX}]
  # both ENV are set (even if they are conflicting to each other),
  # then we use LINUXBREW_PREFIX, and export nothing.
  true
fi

# where the real brew executable should be
export BREW_EXEC="${LINUXBREW_PREFIX}/bin/brew"
# local linuxbrew install script
export BREW_INSTALL="/usr/lib/linuxbrew-wrapper/install"
# upstream install script
export ORIGIN_INSTALL="https://raw.githubusercontent.com/Homebrew/linuxbrew/go/install"

# ruby ?
if [ ! -x $(which ruby) ]; then
  # we can do nothing without ruby.
  printf "E: ruby interpreter not available, abort.\n"
  false
fi

# root ?
if [ `whoami` = "root" ]; then
  # well, the upstream install script will stop when detected root.
  # However, not stopping users who created linuxbrew for root user.
  printf "W: be careful as root.\n"
fi

# If there is brew instance available, just run it.
if [ -x ${BREW_EXEC} ]; then
  exec ${BREW_EXEC} $@
elif [ -d ${LINUXBREW_PREFIX} ]; then
  printf "E: Linuxbrew directory detected, but it seems broken.\n"
  printf "   Try to remove '${LINUXBREW_PREFIX}' and install again.\n"
  false
fi

# if it comes here, we first need to pull the linuxbrew with git.
ARGS="$@"
if [ -r ${BREW_INSTALL} ]; then
  ruby ${BREW_INSTALL} # install first
  if test -n "$ARGS"; then
    exec ${BREW_EXEC} $@
  fi
else
  printf "I: Downloading linuxbrew install script ...\n"
  first_time_hint      # print hint at first
  ruby -e "$(curl -fsSL ${ORIGIN_INSTALL})" # from upstream readme
  if test -n "$ARGS"; then
    exec ${BREW_EXEC} $@
  fi
fi
